package sk.smoradap.demokt.ui.list

import android.os.Bundle
import sk.smoradap.demokt.R
import sk.smoradap.demokt.service.DemoSchedulers
import sk.smoradap.demokt.service.DemoService
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.base.BasePresenter
import sk.smoradap.demokt.ui.list.contract.ListContract

class ListPresenterImpl(var view: ListContract.ListView?, service: DemoService) :
    BasePresenter(service),
    ListContract.ListPresenter {

    private lateinit var notes: ArrayList<Note>

    override fun onShowDetailsRequested(note: Note, position: Int) {
        view?.showDetails(note, position)
    }

    override fun onEditNoteRequested(note: Note, position: Int) {
        view?.showEditNote(note, position)
    }

    override fun onEditNoteDone(note: Note, position: Int) {
        notes.removeAt(position)
        notes.add(position, note)
        view?.updateNote(note, position)
    }

    override fun onNoteAdded(note: Note) {
        notes.add(note)
        view?.addNewNote(note)
        view?.showEmptyView(false)
    }

    override fun onCreateNewNoteRequested() {
        view?.showAddNewNote()
    }

    override fun onStart() {
        super.onStart()
        if (!::notes.isInitialized) {
            loadNotes()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        view = null
    }

    override fun saveState(): Bundle {
        return Bundle().apply {
            putParcelableArrayList("notes", notes)
        }
    }

    override fun restoreState(state: Bundle?) {
        if (state != null) {
            state.getParcelableArrayList<Note>("notes")?.let {
                notes = it
                view?.updateNotes(notes)
                if(it.isEmpty()) {
                    view?.showEmptyView(true)
                }
            }
        }
    }

    private fun loadNotes() {
        val disposable = service.getAllNotes()
            .subscribeOn(DemoSchedulers.io())
            .observeOn(DemoSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading(true) }
            .doFinally { view?.showLoading(false) }
            .subscribe({
                notes = ArrayList<Note>().apply{ addAll(it)}
                if(it.isEmpty()) {
                    view?.showEmptyView(true)
                } else {
                    view?.updateNotes(notes)
                }
            }, {
                view?.showError(R.string.error)
            })
        compositeDisposable.add(disposable)
    }

    override fun onDeleteNoteRequested(note: Note, position: Int) {
        view?.showConfirmDeleteNote(note, position)
    }

    override fun onDeleteNoteConfirmed(note: Note, position: Int) {
        val disposable = service.deleteNote(note.id ?: -1)
            .subscribeOn(DemoSchedulers.io())
            .observeOn(DemoSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading(true) }
            .doFinally { view?.showLoading(false) }
            .subscribe({
                notes.removeAt(position)
                view?.noteDeleted(note, position)
                if(notes.isEmpty()) {
                    view?.showEmptyView(true)
                }
            }, { view?.showError(R.string.error) })

        compositeDisposable.add(disposable)
    }
}
