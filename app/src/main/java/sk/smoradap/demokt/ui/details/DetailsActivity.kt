package sk.smoradap.demokt.ui.details

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import sk.smoradap.demokt.R
import sk.smoradap.demokt.databinding.ActivityDetailsBinding
import sk.smoradap.demokt.ui.NOTE
import sk.smoradap.demokt.ui.base.BaseActivity
import sk.smoradap.demokt.ui.base.BaseContract


class DetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityDetailsBinding>(
            this,
            R.layout.activity_details
        )
        intent?.extras?.let {
            binding.note = it.getParcelable(NOTE)
        }
        setUpToolbar()

    }

    override fun presenter(): BaseContract.Presenter? = null
}
