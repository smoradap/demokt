package sk.smoradap.demokt.ui.list.recycler

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.list.contract.ListContract

class NotesListAdapter(val presenter: ListContract.ListPresenter) :
    RecyclerView.Adapter<NoteViewHolder>() {

    private val notes = mutableListOf<Note>()

    fun addNote(note: Note) {
        notes.add(note)
        notifyItemInserted(notes.lastIndex)
    }

    fun removeNote(position: Int) {
        notes.removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateNotes(newList: List<Note>) {
        notes.clear()
        notes.addAll(newList)
        notifyDataSetChanged()
    }

    fun updateNote(note: Note, position: Int) {
        notes.removeAt(position)
        notes.add(position, note)
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NoteViewHolder.create(parent)

    override fun getItemCount() = notes.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(notes[position], presenter)
    }
}
