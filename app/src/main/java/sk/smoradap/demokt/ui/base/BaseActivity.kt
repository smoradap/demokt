package sk.smoradap.demokt.ui.base

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.progressbar.*
import sk.smoradap.demokt.R

abstract class BaseActivity : AppCompatActivity(), BaseContract.View {

    companion object {
        const val PRESENTER_STATE = "presenter"
    }

    abstract fun presenter(): BaseContract.Presenter?

    override fun onStart() {
        super.onStart()
        presenter()?.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter()?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter()?.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter()?.saveState()?.let {
            outState.putBundle(PRESENTER_STATE, it)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    protected fun presenterState(savedState: Bundle?) = savedState?.getBundle(PRESENTER_STATE)


    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showError(message: Int) {
        showError(getString(message))
    }

    fun setUpToolbar() {
        findViewById<Toolbar>(R.id.toolbar).let { toolbar ->
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp)
            }
        }
    }

    override fun showLoading(visible: Boolean) {
        progress_bar?.visibility = if (visible) View.VISIBLE else View.GONE
    }
}
