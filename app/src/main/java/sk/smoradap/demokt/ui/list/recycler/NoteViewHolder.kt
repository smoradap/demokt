package sk.smoradap.demokt.ui.list.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sk.smoradap.demokt.R
import sk.smoradap.demokt.databinding.ItemNoteBinding
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.list.contract.ListContract

class NoteViewHolder(private val binding: ItemNoteBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(note: Note, presenter: ListContract.ListPresenter) {
        binding.note = note
        binding.presenter = presenter
        binding.holder = this
    }

    companion object {

        fun create(parent: ViewGroup): NoteViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = DataBindingUtil.inflate<ItemNoteBinding>(
                inflater,
                R.layout.item_note,
                parent,
                false
            )
            return NoteViewHolder(binding)
        }
    }
}
