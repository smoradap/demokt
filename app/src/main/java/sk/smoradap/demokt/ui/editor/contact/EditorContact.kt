package sk.smoradap.demokt.ui.editor.contact

import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.base.BaseContract

interface EditorContact {

    interface View : BaseContract.View {
        fun result(note: Note, position: Int)
    }

    interface Presenter : BaseContract.Presenter {
        fun onSave(text: String, note: Note?, position: Int)
    }

}
