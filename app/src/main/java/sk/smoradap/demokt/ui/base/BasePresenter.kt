package sk.smoradap.demokt.ui.base

import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import sk.smoradap.demokt.service.DemoService

abstract class BasePresenter(val service: DemoService) : BaseContract.Presenter {

    val compositeDisposable = CompositeDisposable()

    override fun onStart() {
    }

    override fun onStop() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
    }

    override fun saveState() = Bundle()

    override fun restoreState(state: Bundle?) {
    }
}
