package sk.smoradap.demokt.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.toolbar.*
import sk.smoradap.demokt.R
import sk.smoradap.demokt.databinding.ActivityListBinding
import sk.smoradap.demokt.injection.Injector
import sk.smoradap.demokt.injection.modules.ListModule
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.NOTE
import sk.smoradap.demokt.ui.POSITION
import sk.smoradap.demokt.ui.REQUEST_EDIT
import sk.smoradap.demokt.ui.REQUEST_NEW
import sk.smoradap.demokt.ui.base.BaseActivity
import sk.smoradap.demokt.ui.base.BaseContract
import sk.smoradap.demokt.ui.details.DetailsActivity
import sk.smoradap.demokt.ui.editor.EditorActivity
import sk.smoradap.demokt.ui.list.contract.ListContract
import sk.smoradap.demokt.ui.list.recycler.NotesListAdapter

class ListActivity : BaseActivity(), ListContract.ListView {

    private lateinit var adapter: NotesListAdapter
    private lateinit var presenter: ListContract.ListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val component = Injector.demoComponent.plusListComponent(ListModule(this))
        presenter = component.getListPresenter()

        val binding =
            DataBindingUtil.setContentView<ActivityListBinding>(this, R.layout.activity_list)
        binding.presenter = presenter

        setSupportActionBar(toolbar)
        initRecycler()
        presenter.restoreState(presenterState(savedInstanceState))
    }

    private fun initRecycler() {
        adapter = NotesListAdapter(presenter)
        recyclerView.apply {
            this.adapter = this@ListActivity.adapter
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val note = data.getParcelableExtra<Note>(NOTE)
            val position = data.getIntExtra(POSITION, -1)
            when (requestCode) {
                REQUEST_NEW -> presenter.onNoteAdded(note)
                REQUEST_EDIT -> presenter.onEditNoteDone(note, position)
            }
        }
    }

    override fun presenter(): BaseContract.Presenter? = presenter

    override fun showDetails(note: Note, position: Int) {
        val intent = Intent(this@ListActivity, DetailsActivity::class.java).apply {
            putExtra(NOTE, note)
        }
        startActivity(intent)
    }

    override fun showEditNote(note: Note, position: Int) {
        val intent = Intent(this, EditorActivity::class.java).apply {
            putExtra(NOTE, note)
            putExtra(POSITION, position)
        }
        startActivityForResult(intent, REQUEST_EDIT)
    }

    override fun showAddNewNote() {
        val intent = Intent(this, EditorActivity::class.java)
        startActivityForResult(intent, REQUEST_NEW)
    }

    override fun noteDeleted(note: Note, position: Int) {
        adapter.removeNote(position)
    }

    override fun updateNote(note: Note, position: Int) {
        adapter.updateNote(note, position)
    }

    override fun addNewNote(note: Note) {
        adapter.addNote(note)
    }

    override fun showConfirmDeleteNote(note: Note, position: Int) {
        presenter.onDeleteNoteConfirmed(note, position)
    }

    override fun updateNotes(list: List<Note>) {
        adapter.updateNotes(list)
    }

    override fun showEmptyView(show: Boolean) {
        tv_empty.visibility = if(show) View.VISIBLE else View.GONE
    }
}
