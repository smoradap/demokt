package sk.smoradap.demokt.ui.editor

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_editor.*
import sk.smoradap.demokt.R
import sk.smoradap.demokt.databinding.ActivityEditorBinding
import sk.smoradap.demokt.injection.Injector
import sk.smoradap.demokt.injection.modules.EditorModule
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.NOTE
import sk.smoradap.demokt.ui.POSITION
import sk.smoradap.demokt.ui.base.BaseActivity
import sk.smoradap.demokt.ui.base.BaseContract
import sk.smoradap.demokt.ui.editor.contact.EditorContact

class EditorActivity : BaseActivity(), EditorContact.View {

    private var note: Note? = null
    private var position = -1
    private lateinit var presenter: EditorContact.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityEditorBinding>(this, R.layout.activity_editor)
        intent?.extras?.let {
            note = it.getParcelable(NOTE)
            binding.note = note
            position = it.getInt(POSITION, -1)
        }
        binding.executePendingBindings()

        setUpToolbar()
        setTitle()

        val component = Injector.demoComponent.plusEditorComponent(EditorModule(this))
        presenter = component.getEditorPresenter()
    }

    private fun setTitle() {
        if (note == null) {
            setTitle(R.string.new_note)
        } else {
            setTitle(R.string.edit)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_edit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.save -> {
                presenter.onSave(et_note.text.toString(), note, position)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun result(note: Note, position: Int) {
        val intent = Intent().apply {
            putExtra(NOTE, note)
            putExtra(POSITION, position)
        }
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun presenter(): BaseContract.Presenter? = presenter
}
