package sk.smoradap.demokt.ui.editor

import sk.smoradap.demokt.R
import sk.smoradap.demokt.service.DemoSchedulers
import sk.smoradap.demokt.service.DemoService
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.base.BasePresenter
import sk.smoradap.demokt.ui.editor.contact.EditorContact

class EditorPresenterImpl(var view: EditorContact.View?, service: DemoService) :
    BasePresenter(service),
    EditorContact.Presenter {

    override fun onSave(text: String, note: Note?, position: Int) {
        if (note == null) {
            createNote(text)
        } else {
            updateNote(text, note, position)
        }
    }

    private fun createNote(text: String) {
        val disposable = service.createNote()
            .subscribeOn(DemoSchedulers.io())
            .observeOn(DemoSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading(true) }
            .doFinally { view?.showLoading(false) }
            .subscribe({ view?.result(it, -1) }, { view?.showError(R.string.error) })
        compositeDisposable.add(disposable)
    }

    private fun updateNote(text: String, note: Note, position: Int) {
        val disposable = service.updateNote(note.id ?: -1, note)
            .subscribeOn(DemoSchedulers.io())
            .observeOn(DemoSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading(true) }
            .doFinally { view?.showLoading(false) }
            .subscribe({ view?.result(it, position) }, { view?.showError(R.string.error) })
        compositeDisposable.add(disposable)
    }
}
