package sk.smoradap.demokt.ui.base

import android.os.Bundle

interface BaseContract {

    interface View {

        fun showError(message: String)
        fun showError(message: Int)
        fun showLoading(visible: Boolean)
    }

    interface Presenter {
        fun onStart()
        fun onStop()
        fun onDestroy()
        fun saveState(): Bundle
        fun restoreState(state: Bundle?)
    }
}
