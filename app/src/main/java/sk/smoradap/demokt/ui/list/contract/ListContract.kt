package sk.smoradap.demokt.ui.list.contract

import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.base.BaseContract

interface ListContract {

    interface ListView : BaseContract.View {
        fun showDetails(note: Note, position: Int)
        fun showEditNote(note: Note, position: Int)
        fun showAddNewNote()
        fun noteDeleted(note: Note, position: Int)
        fun updateNote(note: Note, position: Int)
        fun addNewNote(note: Note)
        fun showConfirmDeleteNote(note: Note, position: Int)
        fun updateNotes(list: List<Note>)
        fun showEmptyView(show: Boolean)
    }

    interface ListPresenter : BaseContract.Presenter {
        fun onShowDetailsRequested(note: Note, position: Int)
        fun onEditNoteRequested(note: Note, position: Int)
        fun onEditNoteDone(note: Note, position: Int)
        fun onCreateNewNoteRequested()
        fun onDeleteNoteRequested(note: Note, position: Int)
        fun onDeleteNoteConfirmed(note: Note, position: Int)
        fun onNoteAdded(note: Note)
    }
}
