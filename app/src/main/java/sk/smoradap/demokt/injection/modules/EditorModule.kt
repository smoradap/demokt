package sk.smoradap.demokt.injection.modules

import dagger.Module
import dagger.Provides
import sk.smoradap.demokt.injection.scope.EditorScope
import sk.smoradap.demokt.service.DemoService
import sk.smoradap.demokt.ui.editor.EditorPresenterImpl
import sk.smoradap.demokt.ui.editor.contact.EditorContact

@Module
class EditorModule(val view: EditorContact.View) {

    @EditorScope
    @Provides
    fun provideEditorPresenter(service: DemoService): EditorContact.Presenter {
        return EditorPresenterImpl(view, service)
    }

}
