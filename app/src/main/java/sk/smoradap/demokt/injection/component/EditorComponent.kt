package sk.smoradap.demokt.injection.component

import dagger.Subcomponent
import sk.smoradap.demokt.injection.modules.EditorModule
import sk.smoradap.demokt.injection.scope.EditorScope
import sk.smoradap.demokt.ui.editor.contact.EditorContact

@Subcomponent(modules = [EditorModule::class])
@EditorScope
interface EditorComponent {

    fun getEditorPresenter(): EditorContact.Presenter
}
