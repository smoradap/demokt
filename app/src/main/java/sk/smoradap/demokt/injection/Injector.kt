package sk.smoradap.demokt.injection

import android.content.Context
import sk.smoradap.demokt.injection.component.DaggerDemoComponent
import sk.smoradap.demokt.injection.component.DemoComponent
import sk.smoradap.demokt.injection.modules.GeneralModule

class Injector {

    companion object {

        lateinit var demoComponent: DemoComponent

        fun init(context: Context) {
            demoComponent = DaggerDemoComponent.builder()
                .generalModule(GeneralModule(context))
                .build()
        }
    }
}
