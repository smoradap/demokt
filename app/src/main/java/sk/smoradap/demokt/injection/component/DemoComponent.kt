package sk.smoradap.demokt.injection.component

import android.content.Context
import com.google.gson.Gson
import dagger.Component
import sk.smoradap.demokt.injection.modules.*
import sk.smoradap.demokt.service.DemoService
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkingModule::class, ConverterModule::class, ServiceModule::class, GeneralModule::class])
interface DemoComponent {

    fun getDemoService(): DemoService
    fun getGson(): Gson
    fun getContext(): Context

    fun plusListComponent(listModule: ListModule): ListComponent
    fun plusEditorComponent(editorModule: EditorModule): EditorComponent

}
