package sk.smoradap.demokt.injection.scope

import javax.inject.Scope

@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class DetailsScope
