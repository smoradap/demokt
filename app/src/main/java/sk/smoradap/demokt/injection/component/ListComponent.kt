package sk.smoradap.demokt.injection.component

import dagger.Subcomponent
import sk.smoradap.demokt.injection.modules.ListModule
import sk.smoradap.demokt.injection.scope.ListScope
import sk.smoradap.demokt.ui.list.contract.ListContract

@Subcomponent(modules = [ListModule::class])
@ListScope
interface ListComponent {

    fun getListPresenter(): ListContract.ListPresenter


}
