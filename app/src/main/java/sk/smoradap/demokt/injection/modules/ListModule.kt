package sk.smoradap.demokt.injection.modules

import dagger.Module
import dagger.Provides
import sk.smoradap.demokt.injection.scope.ListScope
import sk.smoradap.demokt.service.DemoService
import sk.smoradap.demokt.ui.list.ListPresenterImpl
import sk.smoradap.demokt.ui.list.contract.ListContract

@Module
class ListModule(var view: ListContract.ListView) {

    @ListScope
    @Provides
    fun provideListPresenter(service: DemoService): ListContract.ListPresenter {
        return ListPresenterImpl(view, service)
    }

}
