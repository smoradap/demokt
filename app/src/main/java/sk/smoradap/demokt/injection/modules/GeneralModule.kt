package sk.smoradap.demokt.injection.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class GeneralModule(val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context = context

}
