package sk.smoradap.demokt

import android.app.Application
import sk.smoradap.demokt.injection.Injector
import timber.log.Timber

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Injector.init(this)
    }
}
