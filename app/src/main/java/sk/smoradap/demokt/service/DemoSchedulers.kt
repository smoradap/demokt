package sk.smoradap.demokt.service

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object DemoSchedulers {

    fun mainThread() = AndroidSchedulers.mainThread()

    fun io() = Schedulers.io()
}
