package sk.smoradap.demokt.service.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Note(@SerializedName("id") var id: Int? = null, @SerializedName("title") var title: String = "") :
    Parcelable
