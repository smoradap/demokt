package sk.smoradap.demokt.service

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*
import sk.smoradap.demokt.service.models.Note


interface DemoService {

    @GET("/notes")
    fun getAllNotes(): Single<List<Note>>

    @GET("notes/{id}")
    fun getNote(@Path("id") id: Int): Single<Note>

    @POST("/notes")
    fun createNote(): Single<Note>

    @PUT("/notes/{id}")
    fun updateNote(@Path("id") id: Int, @Body note: Note): Single<Note>

    @DELETE("/notes/{id}")
    fun deleteNote(@Path("id") id: Int): Completable
}
