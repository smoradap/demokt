package sk.smoradap.demokt.ui.list

import io.reactivex.Completable
import io.reactivex.Single
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import sk.smoradap.demokt.service.DemoService
import sk.smoradap.demokt.service.models.Note
import sk.smoradap.demokt.ui.list.contract.ListContract
import org.mockito.Mockito.`when` as mockWhen

internal class ListPresenterImplTest {

    private lateinit var notesList: List<Note>
    private lateinit var service: DemoService
    private lateinit var view: ListContract.ListView

    @BeforeEach
    fun setUp() {
        notesList = listOf(Note(0, "Test string"), Note(1, "Whatever I like"))
        service = mock(DemoService::class.java)
        view = mock(ListContract.ListView::class.java)
    }

    @Test
    @DisplayName("Test loading of notes")
    fun testLoadNotes() {
        mockWhen(service.getAllNotes()).thenReturn(Single.just(notesList))

        val presenter = ListPresenterImpl(view, service)
        presenter.onStart()

        verify(view, never()).showError(ArgumentMatchers.anyInt())
        verify(view, times(1)).updateNotes(ArgumentMatchers.anyList())
    }

    @Test
    @DisplayName("Test delete note successfull")
    fun testOnDeleteNoteSuccessfull() {
        mockWhen(service.getAllNotes()).thenReturn(Single.just(notesList))
        mockWhen(service.deleteNote(0)).thenReturn(Completable.complete())

        val presenter = ListPresenterImpl(view, service)
        presenter.onStart()
        presenter.onDeleteNoteConfirmed(notesList[0], 0)

        verify(view).noteDeleted(notesList[0], 0)
        verify(view, never()).showError(ArgumentMatchers.anyInt())
    }

    @Test
    @DisplayName("Test delete note failed")
    fun testOnDeleteNoteFailed() {
        mockWhen(service.getAllNotes()).thenReturn(Single.just(notesList))
        mockWhen(service.deleteNote(0)).thenReturn(Completable.error(RuntimeException()))

        val presenter = ListPresenterImpl(view, service)
        presenter.onStart()
        presenter.onDeleteNoteConfirmed(notesList[0], 0)

        verify(view).showError(ArgumentMatchers.anyInt())
        verify(view, never()).noteDeleted(notesList[0], 0)
    }

    @Test
    @DisplayName("Test showing empty view after deleting all notes")
    fun testShowNoMessageAfterDeleteAll() {
        mockWhen(service.getAllNotes()).thenReturn(Single.just(notesList))
        mockWhen(service.deleteNote(0)).thenReturn(Completable.complete())
        mockWhen(service.deleteNote(1)).thenReturn(Completable.complete())

        val presenter = ListPresenterImpl(view, service)
        presenter.onStart()
        presenter.onDeleteNoteConfirmed(notesList[0], 0)
        presenter.onDeleteNoteConfirmed(notesList[1], 0)

        verify(view).noteDeleted(notesList[0], 0)
        verify(view).noteDeleted(notesList[1], 0)
        verify(view, times(1)).showEmptyView(true)
    }
}
