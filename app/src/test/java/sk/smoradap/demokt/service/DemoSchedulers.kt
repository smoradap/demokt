package sk.smoradap.demokt.service

import io.reactivex.internal.schedulers.ImmediateThinScheduler

object DemoSchedulers {

    fun mainThread() = ImmediateThinScheduler.INSTANCE

    fun io() = ImmediateThinScheduler.INSTANCE
}
