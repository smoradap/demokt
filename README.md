# DemoKT repository

Welcome to my DemoKt project. The project contains a simple android app for managing notes created with the use of simple mock api.

## Important
The mocked API used in this app is outside of my control. It can be disabled anytime!
App is written against mock api. This means that you always get same response! For example if you create new note, a request is sent to API to save your note. As the api is mocked, you will see a different note text in the list as notes are read from api.
Same is true for editing notes...

## Usage

 1. Clone this repository
  `git clone git clone https://smoradap@bitbucket.org/smoradap/demokt.git`
 2. Open project in Android Studio
 3. Build it and run it :)

## Implementation notes
1. I decided to use MVP instead of MVVM due to nature of API, as usage of PagedLists and DiffCallbacks would not be beneficial...
2. RxJava is preferred to LiveData due to usage of MVP architecture
3. MVP is used in conjunction with Data Binding.
4. Dagger2 is used as a DI
5. I used rather simple minimalistic GUI due to lack of my time... On the other hand, layouts can be easily updated without any code changes as long as view ids are respected and data binding entries are present.
6. Position of the item in the list is used very often instead of "note id" due to mocked api  will not return unique ids and I allowed to add more 1 note.
7. When note is created/updated a text from server response is used instead of user's text
8. Fragments are not use here as they would be only an overhead and would not provide any benefit
9. 4 simple tests can be found in sk.smoradap.demokt.ui.list.ListPresenterImplTest
